const path = require("path")
const ReactRefreshTypeScript = require("react-refresh-typescript")
const { HotModuleReplacementPlugin } = require("webpack")
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin")


const mode = process.env.NODE_ENV || "development"

module.exports = {
    mode,

    entry: {
        webapp: [
            "./src/webapp",
            mode === "development" && "webpack-hot-middleware/client",
        ].filter(Boolean),
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].js",
    },

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"],
    },

    devtool: mode === "development" && "eval-source-map",

    module: {
        rules: [
            {
                test: /\.tsx?$/i,
                include: path.resolve(__dirname, "src"),

                loader: "ts-loader",
                options: {
                    configFile: "tsconfig.webpack.json",
                    transpileOnly: true,

                    getCustomTransformers: () => ({
                        before: [
                            mode === "development" && ReactRefreshTypeScript(),
                        ].filter(Boolean),
                    }),
                },
            },
        ],
    },

    plugins: [
        mode === "development" && new HotModuleReplacementPlugin(),
        mode === "development" && new ReactRefreshWebpackPlugin(),
    ].filter(Boolean),
}
