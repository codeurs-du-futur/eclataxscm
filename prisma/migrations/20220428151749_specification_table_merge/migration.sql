/*
  Warnings:

  - The values [CLIENT,PRODUCER] on the enum `User_type` will be removed. If these variants are still used in the database, this will fail.
  - You are about to drop the `Project` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Proposition` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `Project` DROP FOREIGN KEY `Project_clientId_fkey`;

-- DropForeignKey
ALTER TABLE `Proposition` DROP FOREIGN KEY `Proposition_producerId_fkey`;

-- DropForeignKey
ALTER TABLE `Proposition` DROP FOREIGN KEY `Proposition_projectId_fkey`;

-- AlterTable
ALTER TABLE `User` MODIFY `type` ENUM('ProjectManager', 'Maker', 'Vendor', 'TransportCompany') NOT NULL;

-- DropTable
DROP TABLE `Project`;

-- DropTable
DROP TABLE `Proposition`;

-- CreateTable
CREATE TABLE `Specification` (
    `id` VARCHAR(191) NOT NULL,
    `authorId` VARCHAR(191) NOT NULL,
    `projectId` VARCHAR(191) NULL,
    `time` VARCHAR(191) NOT NULL,
    `requirements` VARCHAR(191) NOT NULL,
    `quantity` VARCHAR(191) NOT NULL,
    `cost` VARCHAR(191) NOT NULL,
    `state` ENUM('Pending', 'ToNegociate', 'Accepted', 'Refused') NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Specification` ADD CONSTRAINT `Specification_authorId_fkey` FOREIGN KEY (`authorId`) REFERENCES `User`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Specification` ADD CONSTRAINT `Specification_projectId_fkey` FOREIGN KEY (`projectId`) REFERENCES `Specification`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
