import express from "express"
import path from "path"
import api from "./server/api/api"
import { httpHost, httpPort, mode } from "./server/env"


const app = express()

async function registerRoutes() {
    app.use("/api", api)

    if (mode === "production")
        app.get("/webapp.js", (_, res) => {
            res.sendFile( path.join(process.cwd(), "dist/webapp.js") )
        })

    if (mode === "development") {
        const { webpackDevServer } = await import("./webpack-dev-server")
        app.use(webpackDevServer)
    }

    app.get("*", (_, res) => {
        res.sendFile( path.join(process.cwd(), "index.html") )
    })
}

registerRoutes().then(() => {
    app.listen(httpPort, httpHost, 511, () => {
        console.log("HTTP listening on port " + httpPort + ".")
    })
})
