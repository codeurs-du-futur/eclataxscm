import React from "react"
import { render } from "react-dom"
import WebApp from "./ui/WebApp"


render(
    <WebApp />,
    document.querySelector("#app"),
)
