import { Specification, User } from "@prisma/client"
import { filter, find, last } from "lodash"


export type SpecificationWithAuthorAndPropositions = (Specification & {
    author: User
    propositions: (Specification & {
        author: User
    })[]
})


export function specificationClosed(spec: SpecificationWithAuthorAndPropositions) {
    return Boolean(
        find(spec.propositions, proposition => proposition.state === "Accepted")
    )
}

export function specificationInNegociation(spec: SpecificationWithAuthorAndPropositions) {
    return Boolean(
        find(spec.propositions, proposition => proposition.state === "ToNegociate")
    )
}


export function userIsSpecificationAuthor(spec: Specification, user?: User) {
    return user?.id === spec.authorId
}

export function userHasMadeAProposition(spec: SpecificationWithAuthorAndPropositions, user?: User) {
    return Boolean(
        find(spec.propositions, proposition => userIsSpecificationAuthor(proposition, user))
    )
}

export function userHasWon(spec: SpecificationWithAuthorAndPropositions, user?: User) {
    return Boolean(
        find(spec.propositions, proposition => userIsSpecificationAuthor(proposition, user) && proposition.state === "Accepted")
    )
}

export function userNeedsToNegociate(spec: SpecificationWithAuthorAndPropositions, user?: User) {
    return Boolean(
        find(spec.propositions, proposition => userIsSpecificationAuthor(proposition, user) && proposition.state === "ToNegociate")
    )
}


export function getLatestProposition(spec: SpecificationWithAuthorAndPropositions) {
    return last(spec.propositions)
}

export function getUserPropositions(spec: SpecificationWithAuthorAndPropositions, user?: User) {
    return filter(spec.propositions, proposition => userIsSpecificationAuthor(proposition, user))
}
