import { User } from "@prisma/client"
import React, { createContext, Dispatch, ReactNode, SetStateAction, useContext, useEffect, useState } from "react"
import { useAuthorizationToken } from "./AuthorizationTokenContext"
import { useAxios } from "./AxiosContext"


export type UserContextType = [
    User | null,
    Dispatch<SetStateAction<User | null>>,
]


export const UserContext = createContext<UserContextType | null>(null)

export function useUser() {
    return useContext(UserContext) as UserContextType
}


export function UserContextProvider(
    { children }: { children: ReactNode }
) {

    const userState = useState<User | null>(null)
    const [_, setUser] = userState


    const axios = useAxios()
    const [authorizationToken] = useAuthorizationToken()

    async function fetchUser() {
        try {
            const { data: { user } } = await axios.get("/auth/me")
            setUser(user)
        }
        catch(e) {
            console.error(e)
        }
    }

    useEffect(() => {
        if (!authorizationToken)
            setUser(null)
        else
            fetchUser()
    }, [authorizationToken])


    return (
        <UserContext.Provider value={userState}>
            {children}
        </UserContext.Provider>
    )

}
