import { Box, createTheme, CssBaseline, Stack, ThemeProvider } from "@mui/material"
import { SnackbarProvider } from "notistack"
import React, { useState } from "react"
import { BrowserRouter } from "react-router-dom"
import { AuthorizationTokenContextProvider } from "./AuthorizationTokenContext"
import { AxiosContextProvider } from "./AxiosContext"
import { UserContextProvider } from "./UserContext"
import WebAppBar from "./WebAppBar"
import WebAppDrawer from "./WebAppDrawer"
import WebAppRoutes from "./WebAppRoutes"


const theme = createTheme({

})


export default function WebApp() {

    const [drawerOpen, setDrawerOpen] = useState(false)


    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />

            <BrowserRouter>
                <SnackbarProvider>
                    <AuthorizationTokenContextProvider>
                        <AxiosContextProvider>
                            <UserContextProvider>

                                <Stack
                                    direction="column"
                                    alignItems="stretch"

                                    sx={{
                                        width: "100%",
                                        height: "100%",
                                    }}
                                >
                                    <WebAppBar
                                        setDrawerOpen={setDrawerOpen}
                                    />

                                    <Box
                                        sx={{
                                            flexGrow: 1,
                                            overflow: "auto",
                                        }}
                                    >
                                        <WebAppRoutes />
                                    </Box>
                                </Stack>

                                <WebAppDrawer
                                    open={drawerOpen}
                                    setOpen={setDrawerOpen}
                                />

                            </UserContextProvider>
                        </AxiosContextProvider>
                    </AuthorizationTokenContextProvider>
                </SnackbarProvider>
            </BrowserRouter>
        </ThemeProvider>
    )

}
