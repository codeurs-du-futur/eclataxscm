import { Button, Stack, TextField, Typography } from "@mui/material"
import { useSnackbar } from "notistack"
import React, { useState } from "react"
import { useNavigate } from "react-router-dom"
import { useAuthorizationToken } from "../AuthorizationTokenContext"
import { useAxios } from "../AxiosContext"


export default function Login() {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")


    const axios = useAxios()
    const [_, setAuthorizationToken] = useAuthorizationToken()
    const navigate = useNavigate()
    const { enqueueSnackbar } = useSnackbar()

    async function login() {
        try {
            const { data: { authorizationToken } } = await axios.post("/auth/login", {
                email, password,
            })

            setAuthorizationToken(authorizationToken)
            navigate("/")
        }
        catch(e) {
            console.error(e)
            enqueueSnackbar("Erreur : " + (e as Error).toString(), { variant: "error" })
        }
    }


    return (
        <Stack
            direction="column"
            justifyContent="center"
            alignItems="center"

            sx={{ width: "100%", height: "100%" }}
        >
            <Stack
                direction="column"
                alignItems="strect"
                spacing={2}
            >
                <Typography variant="h5" align="center">Connexion</Typography>

                <TextField
                    fullWidth
                    type="email"
                    variant="outlined"
                    label="Adresse e-mail"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />

                <TextField
                    fullWidth
                    type="password"
                    variant="outlined"
                    label="Mot de passe"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />

                <Stack
                    direction="row"
                    justifyContent="center"
                >
                    <Button
                        variant="contained"
                        onClick={() => login()}
                    >
                        Ok
                    </Button>
                </Stack>
            </Stack>
        </Stack>
    )

}
