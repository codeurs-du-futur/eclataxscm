import { List, ListItem, ListItemText, Stack, Typography } from "@mui/material"
import { User } from "@prisma/client"
import React, { useEffect, useState } from "react"
import { useAxios } from "../AxiosContext"


function Contacts(
    { name, users }: { name: string, users: User[] }
) {

    return <>
        <Typography variant="h6" align="center">{name}</Typography>

        <List>
            {users.map(user =>
                <ListItem key={user.id}>
                    <ListItemText
                        primary={user.name}
                        secondary={user.email}
                    />
                </ListItem>
            )}
        </List>
    </>

}


export default function AddressBook() {

    const [makers, setMakers] = useState<User[]>([])
    const [vendors, setVendors] = useState<User[]>([])
    const [transportCompanies, setTransportCompanies] = useState<User[]>([])

    const axios = useAxios()

    async function fetchAddressBook() {
        try {
            const [
                { data: { addressbook: makersData } },
                { data: { addressbook: vendorsData } },
                { data: { addressbook: transportCompaniesData } },
            ] = await Promise.all([
                axios.get("/addressbook/makers"),
                axios.get("/addressbook/vendors"),
                axios.get("/addressbook/transportcompanies"),
            ])

            setMakers(makersData)
            setVendors(vendorsData)
            setTransportCompanies(transportCompaniesData)
        }
        catch(e) {
            console.error(e)
        }
    }

    useEffect(() => { fetchAddressBook() }, [])


    return <>
        <Typography variant="h5" align="center">Carnet d'adresse</Typography>

        <Contacts
            name="Fabricants"
            users={makers}
        />

        <Contacts
            name="Fournisseurs"
            users={vendors}
        />

        <Contacts
            name="Sociétés de transport"
            users={transportCompanies}
        />
    </>

}
