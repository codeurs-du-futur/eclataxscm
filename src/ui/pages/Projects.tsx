import { Stack, Typography } from "@mui/material"
import { useSnackbar } from "notistack"
import React, { useEffect, useState } from "react"
import { SpecificationWithAuthorAndPropositions } from "../../types/Specification"
import { useAxios } from "../AxiosContext"
import Project from "../elements/projects/Project"
import ProjectForm from "../elements/projects/ProjectForm"
import { useUser } from "../UserContext"


export default function Projects() {

    const [user] = useUser()


    const axios = useAxios()
    const { enqueueSnackbar } = useSnackbar()


    const [allSpecs, setAllSpecs] = useState<SpecificationWithAuthorAndPropositions[]>([])
    const [mySpecs, setMySpecs] = useState<SpecificationWithAuthorAndPropositions[]>([])

    useEffect(() => {
        if (user?.type !== "ProjectManager") {
            axios.get("/specs/all/open")
                .then(({ data: { specs } }) => {
                    setAllSpecs(specs)
                })
                .catch((e: Error) => {
                    enqueueSnackbar("Erreur : " + e.toString(), { variant: "error" })
                })
        }

        axios.get("/specs/all/mine")
            .then(({ data: { specs } }) => {
                setMySpecs(specs)
            })
            .catch((e: Error) => {
                enqueueSnackbar("Erreur : " + e.toString(), { variant: "error" })
            })
    }, [])


    return (
        <Stack
            direction="column"
            justifyContent="center"
            alignItems="center"
            spacing={2}

            sx={{ padding: 2 }}
        >
            {user?.type !== "ProjectManager" ? <>
                <Typography variant="h5" align="center">Appels d'offre</Typography>

                {allSpecs.map(spec =>
                    <Project
                        key={spec.id}
                        spec={spec}
                    />
                )}
            </> : <>
                <Typography variant="h6" align="center">Créer une offre</Typography>

                <ProjectForm
                    route="/submit"
                />
            </>}

            <Typography variant="h5" align="center">Mes projets</Typography>

            {mySpecs.map(spec =>
                <Project
                    key={spec.id}
                    spec={spec}
                />
            )}
        </Stack>
    )

}
