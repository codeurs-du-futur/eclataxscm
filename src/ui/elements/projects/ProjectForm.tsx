import { Button, Stack, TextField } from "@mui/material"
import { Specification } from "@prisma/client"
import { pick } from "lodash"
import React, { useState } from "react"
import { useNavigate } from "react-router-dom"
import { useAxios } from "../../AxiosContext"


export default function ProjectForm(
    {
        route,
        projectId,
    }: {
        route: string
        projectId?: string
    }
) {

    const [spec, setSpec] = useState<Specification>({
        id: "",

        requirements: "",
        quantity: "",
        cost: "",
        time: "",
        state: "Pending",

        authorId: "",
        projectId: projectId || null,

        createdAt: new Date(),
        updatedAt: new Date(),
    })


    const axios = useAxios()
    const navigate = useNavigate()

    async function submit() {
        try {
            await axios.post("/client/" + route, pick(spec, [
                "projectId",
                "requirements",
                "quantity",
                "cost",
                "time",
            ]))

            navigate("/")
            navigate("/projects")
        }
        catch(e) {
            console.error(e)
        }
    }


    return (
        <Stack
            direction="column"
            alignItems="stretch"
            spacing={1}

        >
            <TextField
                variant="outlined"
                label="Cahier des charges"

                multiline

                value={spec.requirements}
                onChange={e => setSpec({ ...spec, requirements: e.target.value })}
            />

            <TextField
                variant="outlined"
                label="Quantité"

                value={spec.quantity}
                onChange={e => setSpec({ ...spec, quantity: e.target.value })}
            />

            <TextField
                variant="outlined"
                label="Coût"

                multiline

                value={spec.cost}
                onChange={e => setSpec({ ...spec, cost: e.target.value })}
            />

            <TextField
                variant="outlined"
                label="Temps"

                multiline

                value={spec.time}
                onChange={e => setSpec({ ...spec, time: e.target.value })}
            />

            <Stack
                direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={1}
            >
                <Button
                    variant="contained"
                    onClick={() => submit()}
                >
                    Ok
                </Button>
            </Stack>
        </Stack>
    )

}
