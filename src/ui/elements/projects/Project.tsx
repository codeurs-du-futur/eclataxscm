import { Button, Paper, Stack, Typography } from "@mui/material"
import { Specification, SpecificationState } from "@prisma/client"
import React, { useMemo } from "react"
import { useNavigate } from "react-router-dom"
import { specificationClosed, specificationInNegociation, SpecificationWithAuthorAndPropositions, userHasWon, userIsSpecificationAuthor, userNeedsToNegociate } from "../../../types/Specification"
import { useAxios } from "../../AxiosContext"
import { useUser } from "../../UserContext"
import ProjectForm from "./ProjectForm"


export default function Project(
    { spec }: {
        spec: SpecificationWithAuthorAndPropositions
    }
) {

    const [user] = useUser()
    const axios = useAxios()
    const navigate = useNavigate()

    const isAuthor = useMemo(() => userIsSpecificationAuthor(spec, user || undefined), [spec, user])

    const closed = useMemo(() => specificationClosed(spec), [spec])
    const inNegociation = useMemo(() => specificationInNegociation(spec), [spec])

    const hasWon = useMemo(() => userHasWon(spec, user || undefined), [spec, user])
    const mustNegociate = useMemo(() => userNeedsToNegociate(spec, user || undefined), [spec, user])


    async function setPropositionState(spec: Specification, state: SpecificationState) {
        try {
            await axios.post("/client/validatePropositions", {
                id: spec.id,
                state,
            })

            navigate("/")
            navigate("/projects")
        }
        catch(e) {
            console.error(e)
        }
    }


    return (
        <Paper
            sx={{
                padding: 4,
                borderRadius: 4,
            }}
        >
            {closed && <Typography variant="h6" align="center">Une proposition a été acceptée, ce projet est terminé</Typography>}
            {hasWon && <Typography variant="h6" align="center">Vous avez remporté cet appel d'offre</Typography>}
            {mustNegociate && <Typography variant="h6" align="center">Votre proposition a été retenue, une phase de négociation a débuté</Typography>}

            <Typography>Par {spec.author.name}</Typography>
            <Typography>Cahier des charges :</Typography>
            <Typography>{spec.requirements}</Typography>
            <Typography>Quantité : {spec.quantity}</Typography>
            <Typography>Coût : {spec.cost}</Typography>
            <Typography>Temps : {spec.time}</Typography>

            {/* {spec.propositions.length > 0 &&

            } */}

            <Typography variant="h6" align="center" sx={{ mt: 1, mb: 1 }}>Propositions</Typography>

            {spec.propositions.map(proposition =>
                <Paper
                    key={proposition.id}
                    sx={{
                        margin: 4,
                        padding: 4,
                        borderRadius: 4,
                    }}
                >
                    <Typography>Proposition de {proposition.author.name}</Typography>
                    <Typography>Cahier des charges :</Typography>
                    <Typography>{proposition.requirements}</Typography>
                    <Typography>Quantité : {proposition.quantity}</Typography>
                    <Typography>Coût : {proposition.cost}</Typography>
                    <Typography>Temps : {spec.time}</Typography>

                    {(!closed && isAuthor && proposition.state === "Pending") &&
                        <Stack
                            direction="row"
                            justifyContent="center"
                            alignItems="center"
                            spacing={1}
                        >
                            {!inNegociation &&
                                <Button
                                    variant="contained"
                                    onClick={() => setPropositionState(proposition, "ToNegociate")}
                                >
                                    Négocier
                                </Button>
                            }

                            <Button
                                variant="contained"
                                onClick={() => setPropositionState(proposition, "Accepted")}
                            >
                                Accepter
                            </Button>

                            <Button
                                variant="contained"
                                onClick={() => setPropositionState(proposition, "Refused")}
                            >
                                Refuser
                            </Button>
                        </Stack>
                    }

                    {proposition.state !== "Pending" &&
                        <Typography align="center" sx={{ padding: 1 }}>
                            <i>
                                {proposition.state === "ToNegociate" && "Proposition marquée comme étant à négocier"}
                                {proposition.state === "Accepted" && "Proposition acceptée"}
                                {proposition.state === "Refused" && "Proposition refusée"}
                            </i>
                        </Typography>
                    }
                </Paper>
            )}

            {(!isAuthor && !closed && (!inNegociation || mustNegociate)) && <>
                <Typography variant="h6" align="center" sx={{ mt: 1, mb: 1 }}>Ajouter une proposition</Typography>

                <ProjectForm
                    route="/suggest"
                    projectId={spec.id}
                />
            </>}
        </Paper>
    )

}
