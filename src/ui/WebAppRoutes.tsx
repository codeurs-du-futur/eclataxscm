import React from "react"
import { Route, Routes } from "react-router-dom"
import AddressBook from "./pages/AddressBook"
import Login from "./pages/Login"
import Projects from "./pages/Projects"
import { useUser } from "./UserContext"


export default function WebAppRoutes() {

    const [user] = useUser()


    return (
        <Routes>
            {user ? <>
                <Route path="/addressbook" element={<AddressBook />} />
                <Route path="/projects" element={<Projects />} />
                <Route path="/" />
            </> : <>
                <Route path="/" element={<Login />} />
            </>}
        </Routes>
    )

}
