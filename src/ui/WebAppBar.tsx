import { AccountBoxSharp, Menu } from "@mui/icons-material"
import { AppBar, Box, Button, IconButton, Toolbar, Tooltip } from "@mui/material"
import React, { Dispatch, SetStateAction } from "react"
import { useNavigate } from "react-router-dom"
import { useAuthorizationToken } from "./AuthorizationTokenContext"
import { useUser } from "./UserContext"


export default function WebAppBar(
    { setDrawerOpen }: {
        setDrawerOpen: Dispatch<SetStateAction<boolean>>
    }
) {

    const [_, setAuthorizationToken] = useAuthorizationToken()
    const [user] = useUser()
    const navigate = useNavigate()


    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton
                    size="large"
                    edge="start"
                    color="inherit"
                    sx={{ mr: 2 }}

                    onClick={() => setDrawerOpen(true)}
                >
                    <Menu />
                </IconButton>

                <Box sx={{ flexGrow: 1 }} />

                {user ?
                    <Tooltip title="Se déconnecter">
                        <Button
                            color="inherit"
                            endIcon={<AccountBoxSharp />}

                            onClick={() => {
                                setAuthorizationToken(null)
                                navigate("/")
                            }}
                        >
                            {user.name}
                        </Button>
                    </Tooltip>
                :
                    <Button
                        color="inherit"
                        onClick={() => navigate("/")}
                    >
                        Connexion
                    </Button>
                }
            </Toolbar>
        </AppBar>
    )

}
