import axios, { AxiosInstance } from "axios"
import React, { createContext, ReactNode, useContext, useMemo } from "react"
import { useAuthorizationToken } from "./AuthorizationTokenContext"


export const AxiosContext = createContext<AxiosInstance | null>(null)

export function useAxios() {
    return useContext(AxiosContext) as AxiosInstance
}


export function AxiosContextProvider(
    { children }: { children: ReactNode }
) {

    const [authorizationToken] = useAuthorizationToken()


    return (
        <AxiosContext.Provider
            value={useMemo(() =>
                axios.create({
                    baseURL: "/api",
                    headers: authorizationToken ?
                        { Authorization: "Bearer " + authorizationToken }
                    :
                        {}
                }),
            [authorizationToken])}
        >
            {children}
        </AxiosContext.Provider>
    )

}
