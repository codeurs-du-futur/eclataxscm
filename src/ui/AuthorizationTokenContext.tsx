import Cookie from "js-cookie"
import React, { createContext, Dispatch, ReactNode, SetStateAction, useContext, useEffect, useState } from "react"


export type AuthorizationTokenContextType = [
    string | null,
    Dispatch<SetStateAction<string | null>>,
]


export const AuthorizationTokenContext = createContext<AuthorizationTokenContextType | null>(null)

export function useAuthorizationToken() {
    return useContext(AuthorizationTokenContext) as AuthorizationTokenContextType
}


export function AuthorizationTokenContextProvider(
    { children }: { children: ReactNode }
) {

    const authorizationTokenState = useState<string | null>(Cookie.get("authorizationCookie") || null)
    const [authorizationToken] = authorizationTokenState

    useEffect(() => {
        if (authorizationToken)
            Cookie.set("authorizationCookie", authorizationToken)
        else
            Cookie.remove("authorizationCookie")
    }, [authorizationToken])


    return (
        <AuthorizationTokenContext.Provider value={authorizationTokenState}>
            {children}
        </AuthorizationTokenContext.Provider>
    )

}
