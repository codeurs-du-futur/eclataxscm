import { Drawer, List, ListItem, ListItemText } from "@mui/material"
import React, { Dispatch, SetStateAction } from "react"
import { NavigateOptions, To, useNavigate } from "react-router-dom"
import { useUser } from "./UserContext"


export default function WebAppDrawer(
    { open, setOpen }: {
        open: boolean
        setOpen: Dispatch<SetStateAction<boolean>>
    }
) {

    const [user] = useUser()

    const reactRouterNavigate = useNavigate()
    function navigate(to: To, options?: NavigateOptions) {
        reactRouterNavigate(to, options)
        setOpen(false)
    }


    return (
        <Drawer
            anchor="bottom"
            open={open}
            onClose={() => setOpen(false)}
        >
            <List>
                {user ? <>

                    <ListItem
                        button
                        onClick={() => navigate("/projects")}
                    >
                        <ListItemText primary="Projets" />
                    </ListItem>

                    <ListItem
                        button
                        onClick={() => navigate("/addressbook")}
                    >
                        <ListItemText primary="Carnet d'adresse" />
                    </ListItem>

                    {user.type === "ProjectManager" && <>
                    </>}

                    {(user.type === "Maker" || user.type === "Vendor" || user.type === "TransportCompany") && <>
                    </>}

                </> : <>

                    <ListItem
                        button
                        onClick={() => navigate("/")}
                    >
                        <ListItemText primary="Connexion" />
                    </ListItem>

                </>}
            </List>
        </Drawer>
    )

}
