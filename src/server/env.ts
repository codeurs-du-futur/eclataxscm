import dotenv from "dotenv"


export const mode = process.env.NODE_ENV || "development"


dotenv.config()

export const secret = process.env.SECRET || ""
if (secret.length < 3) throw "SECRET env variable must be >= 3 characters"

export const httpHost = process.env.HTTP_HOST || ""

export const httpPort = Number(process.env.HTTP_PORT)
if (!Number.isInteger(httpPort)) throw "HTTP_PORT env variable must be an integer"
