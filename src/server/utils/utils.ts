import { NextFunction, Response } from "express"
import { AuthenticatedRequest } from "../middlewares/auth"
import { Validator } from "node-input-validator"

const M = {
  logFn: (fn:Function) => async (req:Request, res:Response) => {
    const result = await fn(req, res);
    console.table(result.flat());
  },

  match: (obj:Object, control:Array<any>) => {
    return control.every((prop:any) => { return obj.hasOwnProperty(prop); });
  },

  extract: (src:any, template:Array<any>) => {
    const data = template.reduce((obj:any, key:any) => ({...obj, [key]: src[key]}), {});
    const missings = M.findMissingFields(data, template);

    return [data, missings];
  },

  findMissingFields: (obj:any, template:any) => {
    let missings = template.filter((key:any) => {return obj[key] == undefined; });
    return (missings == []) ? false : missings;
  },

  validate(pattern: Object) {
    return async (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
      const v = new Validator(req.body, pattern)

      if (!await v.check()) {
        res.status(422).json({ inputErrors: v.errors })
      }

      next()
    }
  },
};

export default M;
