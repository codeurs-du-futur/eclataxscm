import { User } from "@prisma/client"
import { NextFunction, Request, Response } from "express"
import jwt from "jsonwebtoken"
import { secret } from "../env"
import { AuthorizationTokenData } from "../jwt"
import prisma from "../prisma"


/**
 * An express request object with authentification fields.
 */
export type AuthenticatedRequest =
    Request & {
        user?: User
        authorizationTokenData?: AuthorizationTokenData
    }


/**
 * Authenticates a user if the request possesses an authorization token.
 */
export async function authenticateUser(rawReq: Request, res: Response, next: NextFunction) {
    const authorization = rawReq.headers.authorization

    if (authorization) {
        const [authorizationType, authorizationToken] = authorization.split(" ")

        try {
            if (authorizationType !== "Bearer") throw "unsupported authorization method"

            const tokenData = jwt.verify(authorizationToken, secret)
            if (typeof tokenData === "string") throw "invalid payload"

            const user = await prisma.user.findUnique({ where: { id: tokenData.userId } })
            if (!user) throw "user not found"

            const req = rawReq as AuthenticatedRequest
            req.user = user
            req.authorizationTokenData = tokenData as AuthorizationTokenData
        }
        catch(e) {
            res.status(422).json({ error: (e as Error).toString() })
            return
        }
    }

    next()
}


/**
 * Denies access to a route to requests verifying a given predicate.
 * @param predicate Predicates that dictates whether or not the access to the route is denied
 * @param errorCode HTTP status code of the response if access to the route is denied
 * @param error Returned error if access to the route is denied
 * @returns An Express middleware
 */
export function deny(
    predicate: (req: AuthenticatedRequest, res: Response) => Promise<boolean>,
    errorCode: number,
    error: string,
) {
    return async (rawRequest: Request, res: Response, next: NextFunction) => {
        const req = rawRequest as AuthenticatedRequest

        if (await predicate(req, res)) {
            res.status(errorCode).json({ error })
            return
        }

        next()
    }
}

/**
 * Allows access to a route only to requests verifying a given predicate.
 * @param predicate Predicates that dictates whether or not the access to the route is granted
 * @param errorCode HTTP status code of the response if access to the route is denied
 * @param error Returned error if access to the route is denied
 * @returns An Express middleware
 */
export function allow(
    predicate: (req: AuthenticatedRequest, res: Response) => Promise<boolean>,
    errorCode: number,
    error: string,
) {
    return deny(
        async (req, res) => !await predicate(req, res),
        errorCode,
        error,
    )
}


/**
 * Prevents authenticated users to get access to a route.
 */
export const denyAuthenticatedUsers = deny(
    async ({ user }) => Boolean(user),
    403, "authenticated"
)

export const denyProjectManagers = deny(
    async ({ user }) => user?.type === "ProjectManager",
    403, "is project manager"
)


/**
 * Only allow access to a route to authenticated users.
 */
export const allowAuthenticatedUsers = allow(
    async ({ user }) => Boolean(user),
    403, "not authenticated"
)

export const allowProjectManagers = allow(
    async ({ user }) => user?.type === "ProjectManager",
    403, "not project manager"
)
