import { allowAuthenticatedUsers, AuthenticatedRequest } from "../middlewares/auth"
import { NextFunction, Response, Router } from "express"
import prisma from "../prisma"
import utils from "../utils/utils"

const client = Router()

function addSpec(template: Array<string>) {
  return async (req: AuthenticatedRequest, res: Response) => {
    const [data, _] = utils.extract(req.body, template);

    if( req.user ) {
      data.authorId = req.user.id;
      data.state = 'Pending'
      await prisma.specification.create({ data })
      res.json({message: "project added"})
    }
    else {
      res.json({errors: {message: "user not found"}})
    }
  }
}

client.post("/submit",
  allowAuthenticatedUsers,
  utils.validate({
    requirements: "required|string",
    quantity: "required|string",
    time: "required|string",
    cost: "required|string",
  }),

  addSpec(['cost', 'requirements', 'time', 'quantity'])
)

client.post("/suggest",
  allowAuthenticatedUsers,
  utils.validate({
    projectId: "required|string",
  }),

  async (req: AuthenticatedRequest, res: Response, next: NextFunction) => {
    const template = ['projectId', 'cost', 'requirements', 'time', 'quantity']
    const [data, missed] = utils.extract(req.body, template)
    const parentProject = await prisma.specification.findUnique({where: {id: data.projectId}})

    console.log(missed)
    console.log(parentProject)

    if(missed && parentProject)
      missed.forEach( (m:string) => {req.body[m] = (parentProject as any)[m]})

    next();
  },

  addSpec(['projectId', 'cost', 'requirements', 'time', 'quantity'])
)

client.delete("/delete",
  allowAuthenticatedUsers,
  utils.validate({
    id: "required|string",
  }),

  async (req: AuthenticatedRequest, res: Response) => {
    const [data, _] = utils.extract(req.body, ['id'])

    await prisma.specification.delete({ where: {id: data.id} })
    res.json({message: "project deleted"})
  }
)

client.post("/validatePropositions",
  allowAuthenticatedUsers,
  utils.validate({
    id: "required|string",
    state: "required|string",
  }),

  async (req: AuthenticatedRequest, res: Response) => {
    const [queryData, _] = utils.extract(req.body, ['id', 'state'])

    if( req.user ) {
      const pres = await prisma.specification.updateMany({
        where: {
          id: queryData.id,
          project: {
            authorId: req.user.id
          }
        },
        data: {
          state: queryData.state
        }
      })

      console.log(pres)

      res.json({message: "project updated"})
    }
  }
)

export default client
