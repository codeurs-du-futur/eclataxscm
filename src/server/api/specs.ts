import { Router } from "express"
import { AuthenticatedRequest, denyProjectManagers } from "../middlewares/auth"
import prisma from "../prisma"


const specs = Router()

/**
 * Returns all the specs that are still open for propositions.
 */
specs.get("/all/open",
    denyProjectManagers,

    async (_, res) => {
        res.json({
            specs: await prisma.specification.findMany({
                where: {
                    project: null,
                    state: "Pending",
                },

                include: {
                    author: true,

                    propositions: {
                        include: { author: true },

                        orderBy: { createdAt: "desc" },
                    },
                },

                orderBy: { createdAt: "desc" },
            })
        })
    },
)

specs.get("/all/mine",
    async (rawReq, res) => {
        const req = rawReq as AuthenticatedRequest

        const propositions = await prisma.specification.findMany({
            where: { author: req.user },

            include: {
                author: true,

                project: {
                    include: {
                        author: true,

                        propositions: {
                            include: { author: true },

                            orderBy: { createdAt: "desc" },
                        },
                    },
                },

                propositions: {
                    include: { author: true },

                    orderBy: { createdAt: "desc" },
                },
            },

            orderBy: { createdAt: "desc" },
        })

        res.json({
            specs: propositions.map(proposition =>
                proposition.project || proposition
            ),
        })
    },
)


export default specs
