import { json, Router } from "express"
import { authenticateUser } from "../middlewares/auth"
import addressbook from "./addressbook"
import auth from "./auth"
import client from "./client"
import specs from "./specs"


const api = Router()
api.use(json())
api.use(authenticateUser)

api.use("/auth", auth)
api.use("/specs", specs)
api.use("/addressbook", addressbook)
api.use("/client", client)

api.all("*", async (_, res) => {
    res.sendStatus(404)
})


export default api
