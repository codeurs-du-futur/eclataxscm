import { Router } from "express"
import { allowAuthenticatedUsers } from "../middlewares/auth"
import prisma from "../prisma"


const addressbook = Router()

addressbook.get("/makers",
    allowAuthenticatedUsers,

    async (_, res) => {
        res.json({
            addressbook: await prisma.user.findMany({
                where: { type: "Maker" },
            })
        })
    },
)

addressbook.get("/vendors",
    allowAuthenticatedUsers,

    async (_, res) => {
        res.json({
            addressbook: await prisma.user.findMany({
                where: { type: "Vendor" },
            })
        })
    },
)

addressbook.get("/transportcompanies",
    allowAuthenticatedUsers,

    async (_, res) => {
        res.json({
            addressbook: await prisma.user.findMany({
                where: { type: "TransportCompany" },
            })
        })
    },
)


export default addressbook
