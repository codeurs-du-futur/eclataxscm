import argon2 from "argon2"
import { Router } from "express"
import jwt from "jsonwebtoken"
import { omit } from "lodash"
import { Validator } from "node-input-validator"
import { secret } from "../env"
import { allowAuthenticatedUsers, AuthenticatedRequest, denyAuthenticatedUsers } from "../middlewares/auth"
import prisma from "../prisma"


const auth = Router()

auth.get("/me",
    allowAuthenticatedUsers,

    async (req, res) => {
        res.json({
            user: omit((req as AuthenticatedRequest).user, [
                "password",
            ])
        })
    }
)

auth.post("/login",
    denyAuthenticatedUsers,

    async (req, res) => {
        const v = new Validator(req.body, {
            email: "required|email",
            password: "required",
        })

        if (!await v.check()) {
            res.status(422).json({ inputErrors: v.errors })
            return
        }

        const user = await prisma.user.findUnique({ where: { email: req.body.email } })
        if (!user) {
            res.status(422).json({ inputErrors: { email: { message: "No user with this email was found." } } })
            return
        }

        if (!await argon2.verify(user.password, req.body.password)) {
            res.status(422).json({ inputErrors: { password: { message: "Wrong password." } } })
            return
        }

        res.json({ authorizationToken: jwt.sign({ userId: user.id }, secret) })
    },
)


export default auth
