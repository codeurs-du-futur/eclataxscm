import argon2 from "argon2"
import prisma from "./prisma"


async function seed() {
    await prisma.user.createMany({
        data: [
            {
                name: "Maître d'oeuvre",
                email: "projectmanager@mycompany.com",
                password: await argon2.hash("123"),
                type: "ProjectManager",
            },


            {
                name: "Prolians Troiscoups (fabricant)",
                email: "contact@prolians.com",
                password: await argon2.hash("123"),
                type: "Maker",
            },

            {
                name: "ST Macroelectronics (fabricant)",
                email: "contact@st.com",
                password: await argon2.hash("123"),
                type: "Maker",
            },


            {
                name: "ShittyPlastics Inc. (fournisseur)",
                email: "contact@shittyplastics.com",
                password: await argon2.hash("123"),
                type: "Vendor",
            },

            {
                name: "SupperWare (fournisseur)",
                email: "contact@supperware.com",
                password: await argon2.hash("123"),
                type: "Vendor",
            },


            {
                name: "SNCF Logistics (transport)",
                email: "contact@sncf.com",
                password: await argon2.hash("123"),
                type: "TransportCompany",
            },

            {
                name: "Blyat Idi Na Hui SARL (transport)",
                email: "contact@blyatidinahui.ru",
                password: await argon2.hash("123"),
                type: "TransportCompany",
            },
        ],
    })
}

seed()
